##################################################################################################################
#                                                                                                                #
# Variables define the parameterization of Terraform configurations. Variables can be overridden via the CLI.    #
#                                                                                                                #
# Further reading: https://www.terraform.io/docs/configuration/variables.html                                    #
#                                                                                                                #
##################################################################################################################

variable "aws_region" {
  description = "AWS region name"
  default     = "eu-west-1"
}

variable "environment" {
  description = "Environment name"
}

variable "service_name" {
  description = "Name of project or service that this VPC infrastructure relates to."
  default     = "management"
}

variable "vpc_cidr_block" {
  description = "The CIDR block which has been allocated to this VPC"
}

#variable "peer_owner_id" {
#  description = "Peer Owner ID"
#}

#variable "peer_vpc_id" {
#  description = "Peer VPC ID"
#}

#variable "peer_cidr_block" {
#  description = "Peer CIDR Block"
#}

variable "auto_accept" {
  description = "Should we auto accept conenctions?"
  default     = "true"
}

variable "az_limit" {
  description = "The number of availability zones to use"
  default     = 3
}

variable "zone_name" {
  description = "The name of the internal zone"
  default     = "digital.internal"
}

variable "force_destroy" {
  description = "Should we destroy the zone when the VPC is destroyed"
  default     = true
}

variable "local_peer" {
  description = "Is the peer local (same account)?"
  default     = true
}

variable "Test" {
  default = ""
  description = "Test Variable"
}
