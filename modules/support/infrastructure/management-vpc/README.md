# Management VPC

This code is used to create the mangement VPC (one per account).

The following variables are defined (and can be overloaded)

1. aws_region - The region to buld in (default: eu-west-1)
2. environment - Which environment to build
3. service_name - The name of the service that is being built (default: mangement)
4. vpc_cidr_block - The IP block for the management VPC
5. auto_account - If we should auto account VPC peering requests (default: true)
6. az_limit - The number of availability zones to use (default: 3)
7. zone_name - The name of the route53 internal zone to create (default: digital.internal)
8. force_destroy - Should we destroy the internal zone if we destroy the mangement VPC (default: true)
9. local_peer - Is the peering connection coming from the same account? (default: true)

All of these defaults should do everything that is required so make sure you know what you are doing if you decide to override these settings. 

