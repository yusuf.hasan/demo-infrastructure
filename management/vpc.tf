module "vpc" {
  source = "../modules/support/infrastructure/management-vpc"

  # VPC
  environment     = "development"
  vpc_cidr_block  = "172.16.12.0/22"
}
